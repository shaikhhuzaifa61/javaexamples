package com.db.application;

public class Applicationimpl implements iapp{
    private  double balance =0;
    @Override
    public String getversion() {
        return "1.11";
    }

    @Override
    public double deposit(String accountid, double amount) {
        return this.balance+amount;
    }

    @Override
    public double withdraw(String accountid, double amount) {
        return this.balance-amount;
    }
}
