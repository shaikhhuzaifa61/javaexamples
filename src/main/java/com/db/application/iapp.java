package com.db.application;

public interface iapp {

    String getversion();

    double deposit(String accountid, double amount);

    double withdraw(String accountid, double amount);

}
